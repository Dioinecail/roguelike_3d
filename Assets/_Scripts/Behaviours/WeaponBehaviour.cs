﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnWeaponChanged(Weapon weapon);

public class WeaponBehaviour : MonoBehaviour
{
    public static event OnWeaponChanged onWeaponChanged;

    public List<Weapon> carriedWeapons;
    public Weapon selectedWeapon;
    public int selectedWeaponIndex = 0;
    public Vector3 weaponHoldOffset;

    private GameObject selectedWeaponObject;

    public Button swapWeaponButton;

    private void Start()
    {
        if (selectedWeapon == null)
            ChangeWeapon();
    }

    private void Update()
    {
        if(InputManager.Instance.GetButtonDown(swapWeaponButton))
        {
            ChangeWeapon();
        }
    }

    private void ChangeWeapon()
    {
        selectedWeaponIndex++;

        if (selectedWeaponIndex >= carriedWeapons.Count)
            selectedWeaponIndex = 0;

        selectedWeapon = carriedWeapons[selectedWeaponIndex];

        Destroy(selectedWeaponObject);

        selectedWeaponObject = Instantiate(selectedWeapon.weaponPrefab, 
            transform.position + Quaternion.LookRotation(transform.forward, Vector3.up) * weaponHoldOffset, 
            Quaternion.LookRotation(transform.forward, Vector3.up),
            transform);

        onWeaponChanged?.Invoke(selectedWeapon.GetWeapon());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position + Quaternion.LookRotation(transform.forward, Vector3.up) * weaponHoldOffset, 0.25f);
    }
}