﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingBehaviour : AbstractBehaviour
{
    public Button shootButton;

    public Weapon equippedWeapon;

    public Vector3 shootOffset;

    private bool canShoot = true;
    private bool isReloading = false;

    void Update()
    {
        if (equippedWeapon == null)
            return;

        switch (equippedWeapon.type)
        {
            case WeaponType.Charged:
                if (InputManager.Instance.GetButtonUp(shootButton) || Input.GetMouseButtonUp(0))
                {
                    AttemptShoot();
                }
                break;
            case WeaponType.Single:
                if (InputManager.Instance.GetButton(shootButton) || Input.GetMouseButton(0))
                {
                    AttemptShoot();
                }
                break;
            case WeaponType.Auto:
                if (InputManager.Instance.GetButton(shootButton) || Input.GetMouseButton(0))
                {
                    AttemptShoot();
                }
                break;
            case WeaponType.SemiAuto:
                break;
            default:
                break;
        }
    }

    private void AttemptShoot()
    {
        if (!canShoot || isReloading)
            return;

        if(equippedWeapon.currentClipAmmo == 0)
        {
            AttemptReload();
            return;
        }

        OnShoot();
    }

    private void OnShoot()
    {
        // spawn prefab and stuff

        GameObject newProjectile = Instantiate(equippedWeapon.ammoPrefab, transform.position + Quaternion.LookRotation(transform.forward, Vector3.up) * shootOffset, Quaternion.LookRotation(transform.forward, Vector3.up));

        Projectile proj = newProjectile.GetComponent<Projectile>();
        proj.SetDamage(equippedWeapon.damage);

        // obscure code
        proj.rBody.velocity = transform.forward * equippedWeapon.speed;
        equippedWeapon.currentAmmoCount--;
        equippedWeapon.currentClipAmmo--;

        if (equippedWeapon.currentClipAmmo > 0)
        {
            StartCoroutine(ShootCoroutine());
            return;
        }
    }

    private void AttemptReload()
    {
        if (isReloading)
            return;
        StartCoroutine(ReloadCoroutine());
    }

    public void SetWeapon(Weapon w)
    {
        equippedWeapon = w;
    }

    private void OnEnable()
    {
        WeaponBehaviour.onWeaponChanged += SetWeapon;
    }

    private void OnDisable()
    {
        WeaponBehaviour.onWeaponChanged -= SetWeapon;
    }

    private IEnumerator ShootCoroutine()
    {
        canShoot = false;

        yield return new WaitForSeconds(1 / equippedWeapon.fireRate);

        canShoot = true;
    }

    private IEnumerator ReloadCoroutine()
    {
        isReloading = true;

        float oneAmmoReloadTime = equippedWeapon.reloadTime / equippedWeapon.clipAmmo;

        while (equippedWeapon.currentClipAmmo < equippedWeapon.clipAmmo)
        {
            yield return new WaitForSeconds(oneAmmoReloadTime);
            equippedWeapon.currentClipAmmo++;
        }

        isReloading = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position + Quaternion.LookRotation(transform.forward, Vector3.up) * shootOffset, Vector3.one * 0.25f);
    }
}