﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBehaviour : MonoBehaviour
{
    protected Rigidbody rBody;

    protected virtual void Awake()
    {
        rBody = GetComponent<Rigidbody>();
    }
}