﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBehaviour : AbstractBehaviour
{
    public float moveSpeed = 5;
    public float runMultiplier = 1.35f;

    public float acceleration = 3;

    public float minimumMovementThreshold = 0.2f;
    public float runTimeThreshold = 3.5f;

    private Vector3 currentMovementDirection;
    private float runTimerValue = 0;

    void Update()
    {
        Move();
    }

    private void Move()
    {
        currentMovementDirection = InputManager.Instance.movementVector;

        if(currentMovementDirection.magnitude < minimumMovementThreshold)
        {
            runTimerValue = 0;
            return;
        }

        runTimerValue += Time.deltaTime;

        rBody.velocity = Vector3.Lerp(rBody.velocity, currentMovementDirection * moveSpeed * (runTimerValue > runTimeThreshold ? runMultiplier : 1) , acceleration * Time.deltaTime);
    }
}