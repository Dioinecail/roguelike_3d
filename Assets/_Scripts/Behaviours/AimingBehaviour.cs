﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingBehaviour : AbstractBehaviour
{
    public float returnLerpSpeed = 3f;
    public float minimumAimThreshold = 0.2f;
    private Vector3 currentAimingDirection;

    private Vector3 inputAimingVector;
    private Vector3 inputAimingPosition;

    private bool isJoystick = false;

    private void Update()
    {
        Aim();
    }

    private void Aim()
    {
        if (inputAimingPosition.magnitude < InputManager.Instance.aimingPosition.magnitude)
            isJoystick = false;

        inputAimingVector = InputManager.Instance.aimingVector;
        inputAimingPosition = InputManager.Instance.aimingPosition;

        if (inputAimingVector.magnitude > 0.1f)
            isJoystick = true;
        if (isJoystick)
        {
            currentAimingDirection = InputManager.Instance.aimingVector;

            if (currentAimingDirection.magnitude < minimumAimThreshold)
            {
                currentAimingDirection = InputManager.Instance.movementVector;

                if (currentAimingDirection.magnitude < minimumAimThreshold)
                    currentAimingDirection = transform.forward;

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(currentAimingDirection), returnLerpSpeed * Time.deltaTime);
                return;
            }

            transform.rotation = Quaternion.LookRotation(currentAimingDirection, Vector3.up);
        }
        else
        {
            currentAimingDirection = inputAimingPosition - transform.position;
            currentAimingDirection.y = transform.position.y;

            transform.rotation = Quaternion.LookRotation(currentAimingDirection, Vector3.up);
        }
    }
}