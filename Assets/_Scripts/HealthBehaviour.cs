﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBehaviour : MonoBehaviour
{
    private float _hp;
    public float HP
    {
        get
        {
            return _hp;
        }
        set
        {
            if (value >= maxHP)
                _hp = maxHP;
            else if (value <= 0)
            {
                _hp = 0;
                OnDie();
            }
            else
                _hp = value;
        }
    }

    public float maxHP;

    private void Awake()
    {
        HP = maxHP;
    }

    public void TakeDamage(float amount)
    {
        HP -= amount;
    }

    public void Heal(float amount)
    {
        HP += amount;
    }

    public void OnDie()
    {
        Debug.Log(name + "Has died!");
        Destroy(gameObject);
    }
}