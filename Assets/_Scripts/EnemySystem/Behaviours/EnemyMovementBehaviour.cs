﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementBehaviour : EnemyAbstractBehaviour
{
    public float speed = 1f;
    public float acceleration = 0.5f;

    private void Update()
    {
        if (target != null)
            MoveTo();
    }

    private void MoveTo()
    {
        rBody.velocity = Vector3.Lerp(rBody.velocity, transform.forward * speed, acceleration * Time.deltaTime);
    }
}