﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAimingBehaviour : EnemyAbstractBehaviour
{
    public event Action onReady;
    public event Action onNotReady;

    public float aimSpeed = 0.5f;
    public float minimumShootingAngle = 5;

    private Vector3 aimingDirection;

    private void Update()
    {
        if (target != null)
            AimTo(target);
    }

    private void AimTo(Transform t)
    {
        aimingDirection = t.position - transform.position;
        aimingDirection.y = transform.position.y;

        Quaternion targetRotation = Quaternion.LookRotation(aimingDirection);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, aimSpeed * Time.deltaTime);

        if(Vector3.Angle(aimingDirection, transform.forward) < minimumShootingAngle)
        {
            onReady?.Invoke();
        }
        else
        {
            onNotReady?.Invoke();
        }
    }
}