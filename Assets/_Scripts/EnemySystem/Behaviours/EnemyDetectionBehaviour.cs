﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnFoundTarget(Transform t);

public class EnemyDetectionBehaviour : EnemyAbstractBehaviour
{
    public event OnFoundTarget onFoundTarget;

    public float visionRange = 5;
    public LayerMask detectionMask;

    private Collider[] player;

    protected override void Awake()
    {
        base.Awake();
        player = new Collider[1];
    }

    private void Update()
    {
        DetectEnemies();
    }

    private void DetectEnemies()
    {
        int playersFound = Physics.OverlapSphereNonAlloc(transform.position, visionRange, player, detectionMask);

        if(player != null && player.Length > 0 && player[0] != null)
        {
            if(target == null)
            {
                target = player[0].transform;
                onFoundTarget?.Invoke(target);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, visionRange);
    }
}