﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAbstractBehaviour : MonoBehaviour
{
    protected Rigidbody rBody;
    protected Transform target;

    protected virtual void Awake()
    {
        rBody = GetComponent<Rigidbody>();
    }

    public void SetTarget(Transform t)
    {
        target = t;
    }
}