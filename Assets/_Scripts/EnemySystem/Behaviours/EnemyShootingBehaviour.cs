﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingBehaviour : EnemyAbstractBehaviour
{
    private Weapon equippedWeapon;
    public Weapon selectedWeapon;

    public Vector3 shootOffset;

    private bool canShoot = true;
    private bool isReloading = false;
    private bool isReady = false;

    private EnemyAimingBehaviour aimingBehaviour;

    protected override void Awake()
    {
        base.Awake();

        equippedWeapon = selectedWeapon.GetWeapon();

        Instantiate(selectedWeapon.weaponPrefab,
            transform.position + Quaternion.LookRotation(transform.forward, Vector3.up) * shootOffset,
            Quaternion.LookRotation(transform.forward, Vector3.up),
            transform);

        aimingBehaviour = GetComponent<EnemyAimingBehaviour>();
    }

    private void Update()
    {
        if (target != null)
            AttempShoot();
    }

    private void AttempShoot()
    {
        if (!canShoot || isReloading || !isReady)
            return;

        if (equippedWeapon.currentClipAmmo == 0)
        {
            AttemptReload();
            return;
        }

        OnShoot();
    }

    private void OnShoot()
    {
        // spawn prefab and stuff
        GameObject newProjectile = Instantiate(equippedWeapon.ammoPrefab, transform.position + Quaternion.LookRotation(transform.forward, Vector3.up) * shootOffset, Quaternion.LookRotation(transform.forward, Vector3.up));

        Projectile proj = newProjectile.GetComponent<Projectile>();
        proj.SetDamage(equippedWeapon.damage);

        // obscure code
        proj.rBody.velocity = transform.forward * equippedWeapon.speed;
        equippedWeapon.currentAmmoCount--;
        equippedWeapon.currentClipAmmo--;

        Destroy(newProjectile, 15);

        if (equippedWeapon.currentClipAmmo > 0)
        {
            StartCoroutine(ShootCoroutine());
            return;
        }
    }

    private void AttemptReload()
    {
        if (isReloading)
            return;
        StartCoroutine(ReloadCoroutine());
    }

    private void OnReady()
    {
        isReady = true;
    }

    private void OnNotReady()
    {
        isReady = false;
    }

    private void OnEnable()
    {
        aimingBehaviour.onReady += OnReady;
        aimingBehaviour.onNotReady += OnNotReady;
    }

    private void OnDisable()
    {
        aimingBehaviour.onReady += OnReady;
        aimingBehaviour.onNotReady -= OnNotReady;
    }

    private IEnumerator ShootCoroutine()
    {
        canShoot = false;

        yield return new WaitForSeconds(1 / equippedWeapon.fireRate);

        canShoot = true;
    }

    private IEnumerator ReloadCoroutine()
    {
        isReloading = true;

        float oneAmmoReloadTime = equippedWeapon.reloadTime / equippedWeapon.clipAmmo;

        while (equippedWeapon.currentClipAmmo < equippedWeapon.clipAmmo)
        {
            yield return new WaitForSeconds(oneAmmoReloadTime);
            equippedWeapon.currentClipAmmo++;
        }

        isReloading = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position + Quaternion.LookRotation(transform.forward, Vector3.up) * shootOffset, Vector3.one * 0.25f);
    }
}