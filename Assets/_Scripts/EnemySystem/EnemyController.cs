﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public EnemyDetectionBehaviour detectionBehaviour;

    public EnemyAbstractBehaviour[] behaviours;

    private void OnFoundTarget(Transform t)
    {
        foreach (EnemyAbstractBehaviour behaviour in behaviours)
        {
            behaviour.SetTarget(t);
        }
    }

    private void OnEnable()
    {
        detectionBehaviour.onFoundTarget += OnFoundTarget;
    }

    private void OnDisable()
    {
        detectionBehaviour.onFoundTarget -= OnFoundTarget;
    }
}