﻿namespace GAME
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayerController : MonoBehaviour
    {
        void Start()
        {
            CAMERA.CameraController.Instance.SetTarget(transform);
        }
    }
}