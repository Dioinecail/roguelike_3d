﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHelper : MonoBehaviour
{
    public GameObject target;

    public void LayoutWall()
    {
        TraverseTransform(target.transform, "Wall_", LayoutWallBlock);
    }

    public void LayoutWallBlock(Transform t)
    {
        Vector3 position = t.transform.localPosition;

        position.x = Mathf.FloorToInt(position.x);
        position.x += 0.5f;

        position.y = 0.5f;

        position.z = Mathf.FloorToInt(position.z);
        position.z += 0.5f;

        t.transform.localPosition = position;
    }

    public void TraverseTransform(Transform t, string tag, Action<Transform> action)
    {
        if (t.name.Contains(tag))
        {
            action.Invoke(t);
        }

        for (int i = 0; i < t.childCount; i++)
        {
            TraverseTransform(t.GetChild(i), tag, action);
        }
    }
}