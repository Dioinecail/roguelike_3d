﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelHelper))]
public class LevelHelperEditor : Editor
{
    private LevelHelper helper;

    private void OnEnable()
    {
        helper = (LevelHelper)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        if(GUILayout.Button("Layout wall"))
        {
            helper.LayoutWall();
        }
    }
}