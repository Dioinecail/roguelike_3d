﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Block
{
    public Vector3 position;
    public BlockType type;
}

public enum BlockType
{
    Empty,
    FullBlock,
    HalfBlock
}