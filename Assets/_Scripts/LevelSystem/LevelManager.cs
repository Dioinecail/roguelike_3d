﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public Transform levelsRoot; // root object of the levels
    public int sizeX, sizeY;     // size of a level layout
    public int levelSize;        // size of a room
    public int spaceSize;        // space between rooms
    public Level[,] rooms;

    public List<GameObject> roomPrefabs;

    private void Awake()
    {
        rooms = new Level[sizeX, sizeY];

        for (int x = 0; x < rooms.GetLength(0); x++)
        {
            for (int y = 0; y < rooms.GetLength(1); y++)
            {
                rooms[x, y] = GenerateRoom();
                rooms[x, y].levelObject.transform.position = new Vector3(x * (levelSize + spaceSize), 0, y * (levelSize + spaceSize));
            }
        }
    }

    private Level GenerateRoom()
    {
        Level level = new Level();

        level.levelObject = Instantiate(roomPrefabs[Random.Range(0, roomPrefabs.Count)], levelsRoot);

        return level;
    }
}