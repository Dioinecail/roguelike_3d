﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level
{
    public GameObject levelObject;
    public LevelType type;
}

public enum LevelType
{
    Empty,
    Enemies,
    Chest
}