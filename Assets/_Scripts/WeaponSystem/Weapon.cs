﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Weapon : ScriptableObject
{
    public int ammoCount;
    public int currentAmmoCount;

    public int clipAmmo;
    public int currentClipAmmo;

    public float speed;
    public int damage;
    public float reloadTime;
    public float fireRate;

    public WeaponType type;
    public GameObject ammoPrefab;
    public GameObject weaponPrefab;

    public void Reset()
    {
        currentAmmoCount = ammoCount;
        currentClipAmmo = clipAmmo;
    }

    public Weapon GetWeapon()
    {
        Weapon newWeapon = CreateInstance<Weapon>();
        newWeapon.ammoCount = ammoCount;
        newWeapon.currentAmmoCount = currentAmmoCount;

        newWeapon.clipAmmo = clipAmmo;
        newWeapon.currentClipAmmo = currentClipAmmo;

        newWeapon.speed = speed;
        newWeapon.damage = damage;

        newWeapon.reloadTime = reloadTime;
        newWeapon.fireRate = fireRate;

        newWeapon.type = type;
        newWeapon.ammoPrefab = ammoPrefab;
        newWeapon.weaponPrefab = weaponPrefab;

        return newWeapon;
    }
}

public enum WeaponType
{
    Charged,
    Single,
    Auto,
    SemiAuto
}