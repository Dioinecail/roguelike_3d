﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    private static WeaponManager instance;

    public static WeaponManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<WeaponManager>();

                if (instance == null)
                {
                    GameObject manager = new GameObject(typeof(WeaponManager).ToString());
                    manager.AddComponent<WeaponManager>();
                }
            }

            return instance;
        }
    }

    public List<Weapon> weapons = new List<Weapon>();

    private void OnApplicationQuit()
    {
        foreach (Weapon weapon in weapons)
        {
            weapon.Reset();
        }    
    }
}