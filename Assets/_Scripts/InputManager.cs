﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager instance;

    public static InputManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<InputManager>();

                if(instance == null)
                {
                    GameObject manager = new GameObject(typeof(InputManager).ToString());
                    manager.AddComponent<InputManager>();
                }
            }

            return instance;
        }
    }

    private Camera mainCamera;
    private Camera MainCamera
    {
        get
        {
            if (mainCamera == null)
                mainCamera = Camera.main;

            return mainCamera;
        }
    }

    public LayerMask mouseProjectionMask;

    public Vector3 movementVector;
    public Vector3 aimingVector;
    public Vector3 aimingPosition;

    void Update()
    {
        float moveX = Input.GetAxis("Horizontal");           
        float moveY = Input.GetAxis("Vertical");

        movementVector = new Vector3(moveX, 0, moveY);

        float aimX = Input.GetAxis("RightJoystick X");
        float aimY = Input.GetAxis("RightJoystick Y");

        aimingVector = new Vector3(aimX, 0, aimY);

        Ray mouseRay = MainCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(mouseRay, out hit, 100, mouseProjectionMask))
        {
            if (hit.collider != null)
            {
                aimingPosition = hit.point;
            }
        }
    }
    
    public bool GetButtonDown(Button button)
    {
        return Input.GetButtonDown(button.ToString());
    }

    public bool GetButton(Button button)
    {
        return Input.GetButton(button.ToString());
    }

    public bool GetButtonUp(Button button)
    {
        return Input.GetButtonUp(button.ToString());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        //Gizmos.DrawLine(MainCamera.transform.position, MainCamera.transform.position + MainCamera.ScreenPointToRay(Input.mousePosition).direction);
        //Gizmos.DrawWireCube(MainCamera.transform.position + MainCamera.ScreenPointToRay(Input.mousePosition).direction, Vector3.one * 0.5f);

        Ray mouseRay = MainCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if(Physics.Raycast(mouseRay, out hit , 100, mouseProjectionMask))
        {
            if(hit.collider != null)
            {
                Vector3 mousePosition = hit.point;
                Gizmos.DrawLine(MainCamera.transform.position, mousePosition);
            }
        }
    }
}

public enum Button
{
    A,
    B,
    X,
    Y,
    RightBumper,
    LeftBumper
}

public enum Axis
{
    Up,
    Down,
    Left,
    Right,
    RightTrigger,
    LeftTrigger
}