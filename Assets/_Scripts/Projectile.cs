﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Rigidbody rBody;
    public float damage;

    public void SetDamage(float d)
    {
        damage = d;
    }

    private void OnCollisionEnter(Collision collision)
    {
        HealthBehaviour health = collision.collider.GetComponent<HealthBehaviour>();

        if(health != null)
        {
            health.TakeDamage(damage);
        }

        Destroy(gameObject);
    }
}