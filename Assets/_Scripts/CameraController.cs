﻿namespace CAMERA
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CameraController : MonoBehaviour
    {
        public static CameraController Instance;

        public Vector3 offset;
        public float lerpSpeed;

        public Transform followTarget;

        private Vector3 targetPosition;

        private void Awake()
        {
            Instance = this;
        }

        void FixedUpdate()
        {
            if(followTarget != null)
            {
                Follow(followTarget);
            }
        }

        private void Follow(Transform target)
        {
            targetPosition = target.position + offset;

            transform.position = Vector3.Lerp(transform.position, targetPosition, lerpSpeed * Time.deltaTime);
        }

        public void SetTarget(Transform t)
        {
            followTarget = t;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position - offset, Vector3.one);
            Gizmos.DrawLine(transform.position, transform.position - offset);
        }
    }
}